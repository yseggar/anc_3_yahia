package Model;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class Boat {
    private int posX;
    private int posY;
    private int integrity;
    String type;

    public Boat(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        this.integrity = 100;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getIntegrity() {
        return integrity;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void setIntegrity(int integrity) {
        this.integrity = integrity;
    }
    
    public String getType(){
        return type;
    }

}
