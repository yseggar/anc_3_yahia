package Model;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class MineClassic extends Mine{

    
    public MineClassic(int posX, int posY) {
        super(posX, posY);
        this.type="Classique";
    }
       
}
