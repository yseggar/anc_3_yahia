package Model;

import java.util.Random;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class BoatBig extends Boat{
    
    public BoatBig(int posX, int posY) {
        super(posX, posY);
        this.type = "Big";
    }
   
    public int tir(){
        Random r = new Random();
        int x = r.nextInt(100)+1;
        if(x <= 20){
            return 0;
        }
        else if(x > 50){
            return 2;
        }
        else{
            return 1;
        }
    }
}
