package Model;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class Army {
    private final String name;
    private final List<Boat> armyBoat = new ArrayList<>();
    public Army(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }  
    
    
    private int randomX(){
       Random r = new Random();
        int x = r.nextInt(5)+1;
        return x;
   }
    
    private int randomY(){
       Random r = new Random();
        int y = r.nextInt(5)+1;
        return y;
    }
    
     public void ajoutBoat(){
        
        armyBoat.add(new BoatBig(randomX(),randomY()));
        armyBoat.add(new BoatSmall(randomX(),randomY()));
        armyBoat.add(new BoatSmall(randomX(),randomY()));      
    }
    
    public List getboat(){
         return armyBoat; 
    }
    
    public void listBoat(){
        for(Boat b : armyBoat){
            System.out.println("Position du bateau : "+b.getPosX()+" "+b.getPosY()+" Type : "+b.getType()+" Integrity : "+b.getIntegrity());}
    }
    
}
