package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;


/**
 *
 * @author 3001rimalheirosbarre
 */
public final class Sea extends Observable {
    private Case[][] sea;
    private final int TAILLE = 5;
     
    public List armys = new ArrayList<>(); 
    
    public Sea() {
        this.initSea();
    }
    
    
    public void initSea(){
        sea = new Case[TAILLE][TAILLE];
        for(int i = 0; i < this.TAILLE; ++ i){
            System.out.print(i+1);
            for(int j = 0; j < this.TAILLE; ++j){
                sea = new Case[i][j];
                System.out.print(" |");
            }       
            System.out.println(" |");
        } 
    }

    public Case[][] getSea() {
        return sea;
    }
    
    public void ajoutArmy (String nom){
        armys.add(new Army(nom));       
    }
     
}
