package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class Mine {
    private final int posX;
    private final int posY;
    String type;
    private final List<Mine> mines = new ArrayList<>();
    
    public Mine(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }
    
    public String getType() {
        return type;
    }
    
    private int randomX(){
        Random r = new Random();
        int x = r.nextInt(5)+1;
        return x;
    }
    
    private int randomY(){
        Random r = new Random();
        int y = r.nextInt(5)+1;
        return y;
    }   
    
    public void ajoutMine(){      
        mines.add(new MineAtomic(randomX(),randomY()));
    }
    
    
    
}
