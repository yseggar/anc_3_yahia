package Model;

import java.util.Random;

/**
 *
 * @author 3001rimalheirosbarre
 */
public class BoatSmall extends Boat{
   
    
    public BoatSmall(int posX, int posY) {
        super(posX, posY);
        this.type = "Small";
    }

    public int tir(){
        Random r = new Random();
        int x = r.nextInt(100)+1;
        if(x<=20){
            return 2;
        }
        else if(x > 50){
            return 0;
        }
        else{
            return 1;
        }
    }
}
